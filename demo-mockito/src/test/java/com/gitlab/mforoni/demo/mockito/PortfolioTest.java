package com.gitlab.mforoni.demo.mockito;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.mockito.Mockito;

public class PortfolioTest {
  // https://www.tutorialspoint.com/mockito/mockito_first_application.htm

  @Test
  public void testGetStockService() {}

  @Test
  public void testSetStockService() {}

  @Test
  public void testGetStocks() {}

  @Test
  public void testSetStocks() {

  }

  @Test
  public void testGetMarketValue() {
    // Creates a list of stocks to be added to the portfolio
    final List<Stock> stocks = new ArrayList<Stock>();
    final Stock googleStock = new Stock("1", "Google", 10);
    final Stock microsoftStock = new Stock("2", "Microsoft", 100);
    stocks.add(googleStock);
    stocks.add(microsoftStock);
    // add stocks to the portfolio
    final Portfolio portfolio = new Portfolio();
    final StockService stockService = Mockito.mock(StockService.class);
    portfolio.setStockService(stockService);
    portfolio.setStocks(stocks);
    // mock the behavior of stock service to return the value of various stocks
    Mockito.when(stockService.getPrice(googleStock)).thenReturn(50.00);
    Mockito.when(stockService.getPrice(microsoftStock)).thenReturn(1000.00);
    final double marketValue = portfolio.getMarketValue();
    assertEquals(100500.0, marketValue, 0D);
  }

}
