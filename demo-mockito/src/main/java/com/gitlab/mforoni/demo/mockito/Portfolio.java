package com.gitlab.mforoni.demo.mockito;

import java.util.List;

public class Portfolio {

	private StockService stockService;
	private List<Stock> stocks;

	public StockService getStockService() {
		return stockService;
	}

	public void setStockService(final StockService stockService) {
		this.stockService = stockService;
	}

	public List<Stock> getStocks() {
		return stocks;
	}

	public void setStocks(final List<Stock> stocks) {
		this.stocks = stocks;
	}

	public double getMarketValue() {
		double marketValue = 0.0;
		for (final Stock stock : stocks) {
			marketValue += stockService.getPrice(stock) * stock.getQuantity();
		}
		return marketValue;
	}
}
