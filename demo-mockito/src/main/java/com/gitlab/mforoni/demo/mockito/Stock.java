package com.gitlab.mforoni.demo.mockito;

public class Stock {

	private String stockId;
	private String name;
	private int quantity;

	public Stock(final String stockId, final String name, final int quantity) {
		super();
		this.stockId = stockId;
		this.name = name;
		this.quantity = quantity;
	}

	public String getStockId() {
		return stockId;
	}

	public void setStockId(final String stockId) {
		this.stockId = stockId;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(final int quantity) {
		this.quantity = quantity;
	}
}
