package com.gitlab.mforoni.demo.mockito;

public interface StockService {

	public double getPrice(Stock stock);
}
