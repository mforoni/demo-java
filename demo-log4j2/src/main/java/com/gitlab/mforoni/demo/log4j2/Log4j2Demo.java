package com.gitlab.mforoni.demo.log4j2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Requirements:<br>
 * Log4j 2.4 and greater requires Java 7, versions 2.0-alpha1 to 2.3 required Java 6. Some features
 * require optional dependencies; the documentation for these features specifies the dependencies.
 * <p>
 * see: http://logging.apache.org/log4j/2.x/manual/configuration.html
 */
final class Log4j2Demo {

  private static final Logger LOGGER = LogManager.getLogger(Log4j2Demo.class);

  private Log4j2Demo() {}

  public static void main(final String[] args) {
    int count = 1;
    LOGGER.info("Hello World! ({})", count++);
    LOGGER.warn("This is a warning message ({})", count++);
    LOGGER.error("This is an error message ({})", count++);
  }
}
