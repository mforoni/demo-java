package com.gitlab.mforoni.demo.jsoup;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * A simple example, used on the jsoup website.
 */
final class DemoJsoup {

  private DemoJsoup() {
    throw new AssertionError();
  }

  public static void testWikipedia() throws IOException {
    final Document doc = Jsoup.connect("http://en.wikipedia.org/").get();
    log(doc.title());
    final Elements newsHeadlines = doc.select("#mp-itn b a");
    for (final Element headline : newsHeadlines) {
      log("%s\n\t%s", headline.attr("title"), headline.absUrl("href"));
    }
  }

  public static void test(final String url) throws IOException {
    final Document document = Jsoup.connect(url).get();
    log(document.title());
    for (final Element element : document.getAllElements()) {
      log(element.toString());
    }
  }

  public static void main(final String[] args) {
    try {
      testWikipedia();
      test("http://gamesurf.tiscali.it/recensione/the-legend-of-zelda-ocarina-of-time-c16499.html");
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  private static void log(final String msg, final Object... args) {
    System.out.println(String.format(msg, args));
  }
}
