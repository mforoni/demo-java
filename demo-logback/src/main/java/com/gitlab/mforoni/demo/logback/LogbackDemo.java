package com.gitlab.mforoni.demo.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Logback-classic module requires the presence of slf4j-api.jar and logback-core.jar in addition to
 * logback-classic.jar on the classpath.
 */
final class LogbackDemo {

  private static final Logger LOGGER = LoggerFactory.getLogger(LogbackDemo.class);

  private LogbackDemo() {}

  public static void main(final String[] args) {
    // print internal state
    // final LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
    // StatusPrinter.print(lc);
    int counter = 1;
    LOGGER.debug("Debug level message ({})", counter++);
    LOGGER.info("Hello World! ({})", counter++);
    LOGGER.info("My name is {}. ({})", "Marco", counter++);
    LOGGER.warn("Warning message ({})", counter++);
    LOGGER.error("Error message ({})", counter++);
  }
}
