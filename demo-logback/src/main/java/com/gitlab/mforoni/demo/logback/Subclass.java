package com.gitlab.mforoni.demo.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class Subclass extends Super {

  private static final Logger LOGGER = LoggerFactory.getLogger(Subclass.class);

  Subclass() {
    super();
    LOGGER.info("This is the subclass constructor");
  }
}
