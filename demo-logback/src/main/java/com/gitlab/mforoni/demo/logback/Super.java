package com.gitlab.mforoni.demo.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Super {

  private final Logger logger = LoggerFactory.getLogger(Super.class);
  // static ?

  Super() {
    logger.info("This the super constructor");
  }
}
