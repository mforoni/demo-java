package com.gitlab.mforoni.demo.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class MyLogbackDemo {

  private static final Logger LOGGER = LoggerFactory.getLogger(MyLogbackDemo.class.getSimpleName());

  public static void main(final String[] args) {
    LOGGER.info("This is the main");
    new OtherClass();
    new Subclass();
    new OtherClass();
  }
}
