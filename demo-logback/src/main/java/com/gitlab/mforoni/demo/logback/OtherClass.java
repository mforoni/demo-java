package com.gitlab.mforoni.demo.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class OtherClass {

  private final Logger logger = LoggerFactory.getLogger(OtherClass.class);
  // static ?

  OtherClass() {
    logger.info("This the constructor of another class");
  }
}
