package com.gitlab.mforoni.demo.joda.time;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.Months;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

final class DemoJodaTime {

  private DemoJodaTime() {}

  private static void printCurrentDateTime() {
    final DateTimeFormatter dateFormat =
        DateTimeFormat.forPattern("G,C,Y,x,w,e,E,Y,D,M,d,a,K,h,H,k,m,s,S,z,Z");
    final LocalTime localTime = new LocalTime();
    final LocalDate localDate = new LocalDate();
    final DateTime dateTime = new DateTime();
    final LocalDateTime localDateTime = new LocalDateTime();
    final DateTimeZone dateTimeZone = DateTimeZone.getDefault();
    System.out.println("Current LocalTime : " + localTime.toString());
    System.out.println("Current LocalDate : " + localDate.toString());
    System.out.println("Current DateTime : " + dateTime.toString());
    System.out.println("Current LocalDateTime : " + localDateTime.toString());
    System.out.println("Current DateTimeZone : " + dateTimeZone.toString());
    System.out
        .println("Current LocalDateTime with custom format : " + dateFormat.print(localDateTime));
  }

  private static void printLocalDate(final String str, final String pattern) {
    final DateTimeFormatter format = DateTimeFormat.forPattern(pattern);
    final LocalDate date = LocalDate.parse(str, format);
    System.out.println(
        String.format("Parsing string %s with pattern %s gives LocalDate %s", str, pattern, date));
  }

  private static void printDateTime(final String str) {
    final DateTime dateTime = DateTime.parse(str);
    System.out.println(String.format("Parsing string %s gives DateTime %s", str, dateTime));
  }

  private static void printInterval(final DateTime dt1, final DateTime dt2) {
    System.out.println(String.format("Year Difference of %s, %s is %s", dt1, dt2,
        Years.yearsBetween(dt1, dt2).getYears()));
    System.out.println(String.format("Month Difference of %s, %s is %s", dt1, dt2,
        Months.monthsBetween(dt1, dt2)));
  }

  public static void main(final String[] args) {
    printLocalDate("20140518", "yyyyMMdd");
    printDateTime("2002-01-15");
    printCurrentDateTime();
    printInterval(new DateTime(), DateTime.parse("2000-01-01"));
  }
}
