package com.gitlab.mforoni.demo.jdk7;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

final class FilesDemo {

  private FilesDemo() {}

  private static void start() throws IOException {
    final Path dir = Files.createDirectories(Paths.get("output", "test", "mkdirs"));
    System.out.println("Create directory " + dir);
    final Path path = Paths.get("output", "test", "empty.txt");
    try {
      final Path file = Files.createFile(path);
      System.out.println("Create file " + file);
    } catch (FileAlreadyExistsException e) {
      System.err.println(e.getMessage());
    }
    try (final DirectoryStream<Path> directoryStream =
        Files.newDirectoryStream(Paths.get("output", "test"))) {
      final Iterator<Path> iterator = directoryStream.iterator();
      while (iterator.hasNext()) {
        System.out.println(iterator.next());
      }
    }
    final List<String> lines = Files.readAllLines(path, Charset.forName("utf-8"));
    System.out.println(lines);
  }

  public static void main(final String[] args) {
    try {
      start();
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }
}
