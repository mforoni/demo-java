package com.gitlab.mforoni.demo.commons.csv;

/**
 * @author ashraf
 */
final class CsvReadTest {

  /**
   * @param args
   */
  public static void main(String[] args) {
    final String fileName = "resources/student.csv";
    System.out.println("Read CSV file:");
    CsvFileReader.readCsvFile(fileName);
  }
}
