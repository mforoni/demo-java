package com.gitlab.mforoni.demo.commons.csv;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

final class DemoCommonsCsv {

  private static final String[] HEADER = new String[] {"name", "title", "origin"};

  private static void readCsvFile(final String fileName, final CSVFormat csvFormat,
      final int limit) throws IOException {
    final String text =
        limit == -1 ? " with no limits" : String.format(" with %d rows as limit", limit);
    System.out.println("Reading file " + fileName + text);
    try (final FileReader fileReader = new FileReader(fileName); //
        CSVParser csvParser = new CSVParser(fileReader, csvFormat);) {
      final List<CSVRecord> csvRecords = csvParser.getRecords();
      // Read the CSV file records starting from the second record to skip the header
      for (int i = 0; i < csvRecords.size() && i < limit; i++) {
        final CSVRecord record = csvRecords.get(i);
        System.out.println("row " + i + " = " + record);
      }
    }
  }

  public static void main(final String[] args) {
    try {
      readCsvFile("resources/Stats.csv", CSVFormat.DEFAULT.withHeader(HEADER), 5);
      readCsvFile("resources/Stats.csv", CSVFormat.DEFAULT, 5);
      readCsvFile("resources/Stats.csv",
          CSVFormat.DEFAULT.withHeader(HEADER).withSkipHeaderRecord(),
          5);
    } catch(final Exception ex) {
      ex.printStackTrace();
    }
  }
}
