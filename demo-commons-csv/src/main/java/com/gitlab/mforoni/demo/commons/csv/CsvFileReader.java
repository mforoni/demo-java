package com.gitlab.mforoni.demo.commons.csv;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

/**
 * 
 * @author ashraf_sarhan
 */
final class CsvFileReader {
  // https://examples.javacodegeeks.com/core-java/apache/commons/csv-commons/writeread-csv-files-with-apache-commons-csv-example/
  // CSV file header
  private static final String[] FILE_HEADER_MAPPING =
      {"id", "firstName", "lastName", "gender", "age"};
  // Student attributes
  private static final String STUDENT_ID = "id";
  private static final String STUDENT_FNAME = "firstName";
  private static final String STUDENT_LNAME = "lastName";
  private static final String STUDENT_GENDER = "gender";
  private static final String STUDENT_AGE = "age";

  public static void readCsvFile(final String fileName) {
    // Create a new list of student to be filled by CSV file data
    final List<Student> students = new ArrayList<>();
    // Create the CSVFormat object with the header mapping
    final CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(FILE_HEADER_MAPPING);
    // initialize FileReader object
    // initialize CSVParser object
    try (final FileReader fileReader = new FileReader(fileName); //
        final CSVParser csvFileParser = new CSVParser(fileReader, csvFileFormat);) {
      // Get a list of CSV file records
      final List<CSVRecord> csvRecords = csvFileParser.getRecords();
      // Read the CSV file records starting from the second record to skip the header
      for (int i = 1; i < csvRecords.size(); i++) {
        final CSVRecord record = csvRecords.get(i);
        // Create a new student object and fill his data
        final Student student = new Student(Long.parseLong(record.get(STUDENT_ID)),
            record.get(STUDENT_FNAME), record.get(STUDENT_LNAME), record.get(STUDENT_GENDER),
            Integer.parseInt(record.get(STUDENT_AGE)));
        students.add(student);
      }
      // Print the new student list
      for (final Student student : students) {
        System.out.println(student.toString());
      }
    } catch (final Exception e) {
      System.out.println("Error in CsvFileReader !!!");
      e.printStackTrace();
    }
  }
}
