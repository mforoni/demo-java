package com.gitlab.mforoni.demo.jdk5;

final class UncheckedExceptionDemo {

  static void test() {
    try {
      throw new IllegalStateException("I'm an unchecked exception");
    } catch (Exception e) {
      System.out.println(
          "Unchecked exceptions like " + e.getClass().getSimpleName() + " can be catched.");

    }
  }

  public static void main(final String[] args) {
    test();
  }
}
