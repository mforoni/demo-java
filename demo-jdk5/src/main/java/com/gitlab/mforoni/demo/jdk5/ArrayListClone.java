package com.gitlab.mforoni.demo.jdk5;

import java.util.ArrayList;

final class ArrayListClone {

	private ArrayListClone() {}

	private static void testArrayList() {
		final ArrayList<String> l = new ArrayList<String>();
		l.add("Hello World");
		l.add(" Marco");
		@SuppressWarnings("unchecked")
		final ArrayList<String> cloned = (ArrayList<String>) l.clone();
		cloned.set(0, "Prova");
		System.out.println(cloned);
		System.out.println(l);
	}

	public static void main(final String[] args) {
		testArrayList();
		// List do NOT have a clone method!
	}
}
