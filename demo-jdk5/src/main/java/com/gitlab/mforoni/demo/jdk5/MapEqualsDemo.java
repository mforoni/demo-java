package com.gitlab.mforoni.demo.jdk5;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

final class MapEqualsDemo {

	private static final Logger LOGGER = Logger.getLogger(MapEqualsDemo.class.getSimpleName());

	static final class PrimaryKey {

		private final String table;
		private final Map<String, Object> values;

		PrimaryKey(final String table, final String attribute, final Object value) {
			this.table = table;
			values = new HashMap<String, Object>();
			values.put(attribute, value);
		}

		Map<String, Object> getMap() {
			return values;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((table == null) ? 0 : table.hashCode());
			result = prime * result + ((values == null) ? 0 : values.hashCode());
			return result;
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final PrimaryKey other = (PrimaryKey) obj;
			if (table == null) {
				if (other.table != null) {
					return false;
				}
			} else if (!table.equals(other.table)) {
				return false;
			}
			if (values == null) {
				if (other.values != null) {
					return false;
				}
			} else if (!values.equals(other.values)) {
				return false;
			}
			return true;
		}

		@Override
		public String toString() {
			return table + ", " + values;
		}
	}

	static PrimaryKey toPK(final int idBene) {
		return new PrimaryKey("PPBENI", "IDBENE", idBene);
	}

	static PrimaryKey toPK(final BigDecimal idBene) {
		return new PrimaryKey("PPBENI", "IDBENE", idBene);
	}

	static class DBRow {

		private final String row;

		public DBRow(final String row) {
			this.row = row;
		}

		@Override
		public String toString() {
			return row;
		}
	}

	public static void main(final String[] args) {
		final int idBene = 45;
		final Map<PrimaryKey, DBRow> map = new HashMap<MapEqualsDemo.PrimaryKey, MapEqualsDemo.DBRow>();
		map.put(toPK(new BigDecimal(idBene)), new DBRow("record"));
		LOGGER.info("IDBENE = " + idBene);
		LOGGER.info("MAP = " + map.toString());
		final PrimaryKey pk = toPK(idBene);
		LOGGER.info("PK = " + pk + ", MAP CONTAINS PK = " + map.containsKey(pk));
	}
}
