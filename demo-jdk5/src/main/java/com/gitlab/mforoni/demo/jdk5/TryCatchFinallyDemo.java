package com.gitlab.mforoni.demo.jdk5;

final class TryCatchFinallyDemo {

  private static void start() {
    try {
      System.out.println("try to do something...");
      throw new Exception("I'm the Exception thrown");
    } catch (final Exception ex) {
      System.out.println("I'm the catch block that handle the exception: " + ex.getMessage());
    } finally {
      System.out.println("Finally is executed...");
    }
  }

  public static void main(final String[] args) {
    start();
  }
}
