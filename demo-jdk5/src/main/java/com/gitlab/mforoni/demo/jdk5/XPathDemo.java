package com.gitlab.mforoni.demo.jdk5;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * XPath (XML Path Language) is a query language for selecting nodes from an XML document. The XPath
 * API is included in the JDK (package javax.xml.xpath).
 * <p>
 * See this <a href="http://viralpatel.net/blogs/java-xml-xpath-tutorial-parse-xml/">link</a>.
 * 
 * @author viralpatel.net
 */
final class XPathDemo {

  private static void start() throws FileNotFoundException, ParserConfigurationException,
      IOException, SAXException, XPathExpressionException {
    final FileInputStream fis = new FileInputStream(new File("resources/employees.xml"));
    try {
      final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
      final DocumentBuilder builder = builderFactory.newDocumentBuilder();
      final Document xmlDocument = builder.parse(fis);
      final XPath xPath = XPathFactory.newInstance().newXPath();
      System.out.println("*************************");
      String expression = "/Employees/Employee[@emplid='3333']/email";
      System.out.println(expression);
      final String email = xPath.compile(expression).evaluate(xmlDocument);
      System.out.println(email);
      System.out.println("*************************");
      expression = "/Employees/Employee/firstname";
      System.out.println(expression);
      NodeList nodeList =
          (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
      for (int i = 0; i < nodeList.getLength(); i++) {
        System.out.println(nodeList.item(i).getFirstChild().getNodeValue());
      }
      System.out.println("*************************");
      expression = "/Employees/Employee[@type='admin']/firstname";
      System.out.println(expression);
      nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
      for (int i = 0; i < nodeList.getLength(); i++) {
        System.out.println(nodeList.item(i).getFirstChild().getNodeValue());
      }
      System.out.println("*************************");
      expression = "/Employees/Employee[@emplid='2222']";
      System.out.println(expression);
      final Node node = (Node) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODE);
      if (null != node) {
        nodeList = node.getChildNodes();
        for (int i = 0; null != nodeList && i < nodeList.getLength(); i++) {
          final Node nod = nodeList.item(i);
          if (nod.getNodeType() == Node.ELEMENT_NODE) {
            System.out.println(
                nodeList.item(i).getNodeName() + " : " + nod.getFirstChild().getNodeValue());
          }
        }
      }
      System.out.println("*************************");
      expression = "/Employees/Employee[age>40]/firstname";
      nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
      System.out.println(expression);
      for (int i = 0; i < nodeList.getLength(); i++) {
        System.out.println(nodeList.item(i).getFirstChild().getNodeValue());
      }
      System.out.println("*************************");
      expression = "/Employees/Employee[1]/firstname";
      System.out.println(expression);
      nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
      for (int i = 0; i < nodeList.getLength(); i++) {
        System.out.println(nodeList.item(i).getFirstChild().getNodeValue());
      }
      System.out.println("*************************");
      expression = "/Employees/Employee[position() <= 2]/firstname";
      System.out.println(expression);
      nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
      for (int i = 0; i < nodeList.getLength(); i++) {
        System.out.println(nodeList.item(i).getFirstChild().getNodeValue());
      }
      System.out.println("*************************");
      expression = "/Employees/Employee[last()]/firstname";
      System.out.println(expression);
      nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
      for (int i = 0; i < nodeList.getLength(); i++) {
        System.out.println(nodeList.item(i).getFirstChild().getNodeValue());
      }
      System.out.println("*************************");
    } finally {
      fis.close();
    }
  }

  public static void main(final String[] args) {
    try {
      start();
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }
}
