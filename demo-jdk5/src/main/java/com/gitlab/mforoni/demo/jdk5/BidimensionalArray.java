package com.gitlab.mforoni.demo.jdk5;

final class BidimensionalArray {

	private BidimensionalArray() {}

	public static void main(final String[] args) {
		final int[][] matrix = { { 0, 1, 2, 3, 4 }, { 9, 8, 7, 6, 5 } };
		final int rows = matrix.length;
		final int cols = matrix[0].length;
		System.out.println("Matrix : ");
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		final int[] firstRowOfMatrix = matrix[0];
		System.out.println("Array : ");
		for (final int element : firstRowOfMatrix) {
			System.out.print(element + " ");
		}
		System.out.println();
		final int[] firstColumnOfMatrix = new int[rows];
		for (int i = 0; i < rows; i++) {
			firstColumnOfMatrix[i] = matrix[i][0];
		}
		System.out.println("Array : ");
		for (final int element : firstColumnOfMatrix) {
			System.out.print(element + " ");
		}
	}
}