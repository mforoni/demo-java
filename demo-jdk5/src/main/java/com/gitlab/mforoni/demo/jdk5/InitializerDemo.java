package com.gitlab.mforoni.demo.jdk5;

public final class InitializerDemo {

  // instance initializer
  {
    System.out.println("Instance Initializer");
  }
  // static initializer
  static {
    System.out.println("Static Initializer");
  }

  public static void main(final String[] args) {
    System.out.println("main");
    new InitializerDemo();
  }
}
