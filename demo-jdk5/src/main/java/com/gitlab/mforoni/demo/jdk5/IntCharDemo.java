package com.gitlab.mforoni.demo.jdk5;

final class IntCharDemo {

  public static void main(final String[] args) {
    final String s = "Testing some characters, numbers: 0,1,2,3,4,5 and special chars: %&/()[]";
    for (int i = 0; i < s.length(); i++) {
      char c = s.charAt(i);
      int a = c;
      System.out.println(String.format("%s = %d", c, a));
    }
  }
}
