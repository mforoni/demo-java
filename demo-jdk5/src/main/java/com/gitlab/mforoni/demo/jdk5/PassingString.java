package com.gitlab.mforoni.demo.jdk5;

final class PassingString {

	private PassingString() {}

	public static void main(final String[] args) {
		final String s = "prova";
		edit(s);
		System.out.println(s);
	}

	static void edit(String a) {
		a += "edited";
	}
}
