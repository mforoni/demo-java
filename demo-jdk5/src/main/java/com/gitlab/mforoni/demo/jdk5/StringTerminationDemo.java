package com.gitlab.mforoni.demo.jdk5;

import java.util.Arrays;

final class StringTerminationDemo {

  public static void main(final String[] args) {
    final String s = "Hello world";
    System.out.println(s);
    final char[] arr = s.toCharArray();
    System.out.println(Arrays.toString(arr));
    arr[2] = 0;
    System.out.println(Arrays.toString(arr));
  }
}
