package com.gitlab.mforoni.demo.jdk5;

public enum EnumDemo {
  INSTANCE;

  private static final Long ONE = 1L;
  static {
    System.out.println("In static initialiser ONE = " + ONE);
  }

  private EnumDemo() {
    load();
  }

  private void load() {
    System.out.println("In constructor: ONE = " + ONE); // this is NULL!
  }

  public static void main(String[] args) {
    EnumDemo o = EnumDemo.INSTANCE;
    o.name();
  }
}
