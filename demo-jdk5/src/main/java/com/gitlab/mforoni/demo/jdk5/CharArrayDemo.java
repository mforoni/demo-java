package com.gitlab.mforoni.demo.jdk5;

import java.util.Arrays;

final class CharArrayDemo {

  static void test() {
    final String s = "This is some text for manipulating a char array!";
    char[] a = s.toCharArray();
    int len = a.length;
    final String ss = "This is another variable";
    a[len + 1] = 0; // raise an array index out of bound exception
    a[len] = 'H';
    System.out.println(Arrays.toString(a));
  }

  public static void main(final String[] args) {
    test();
  }
}
