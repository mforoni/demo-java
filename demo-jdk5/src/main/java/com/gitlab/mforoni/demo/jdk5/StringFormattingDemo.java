package com.gitlab.mforoni.demo.jdk5;

final class StringFormattingDemo {

  private StringFormattingDemo() {}

  private static void formatAndPrint(final String s, final Object... args) {
    System.out.println(String.format(s, args));
  }

  public static void main(final String[] args) {
    final long l = 31;
    formatAndPrint("Formatting string containg primitive long %s", l);
    formatAndPrint("Formatting string containg primitive long %d", l);
  }
}
