package com.gitlab.mforoni.demo.jdk5;

/**
 * @author Foroni
 */
final class PassingObject {

	private PassingObject() {}

	public static void main(final String[] args) {
		final CustomStringObject cs = new CustomStringObject();
		System.out.println("Custom String Before: " + cs.str);
		hello(cs);
		System.out.println("Custom String After: " + cs.str);
		final String s = "Native String";
		System.out.println("Native String Before: " + s);
		hello(s);
		System.out.println("Native String After: " + s);
	}

	private static void hello(String t) {
		t = "hello " + t;
	}

	private static void hello(final CustomStringObject o) {
		o.str = "hello " + o.str;
	}
}

class CustomStringObject {

	String str = "Custom String";
}
