# Demo JCIFS

This a demo project to test accessing and reading operations of a NAS's shared folder from both Windows and Linux clients using *JCIFS API*. This is a Java based solution to access a shared folder of a generic server from both Windows and Linux clients.

## About [JCIFS](https://jcifs.samba.org/)

JCIFS is an Open Source client library that implements the CIFS/SMB networking protocol in 100% Java. CIFS is the standard file sharing protocol on the Microsoft Windows platform (e.g. Map Network Drive ...).
