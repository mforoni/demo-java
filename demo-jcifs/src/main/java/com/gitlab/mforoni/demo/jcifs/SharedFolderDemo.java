package com.gitlab.mforoni.demo.jcifs;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;

/**
 * Testng access to and read from shared folder using JCIFS API.
 * <p>
 * JCIFS is an Open Source client library that implements the CIFS/SMB networking protocol in 100%
 * Java. CIFS is the standard file sharing protocol on the Microsoft Windows platform (e.g. Map
 * Network Drive ...). This client is used extensively in production on large Intranets. For more
 * info see the following <a href="https://jcifs.samba.org/">link</a>.
 * 
 * @author Marco Foroni
 */
public final class SharedFolderDemo {

  private SharedFolderDemo() {} // Preventing instantiation

  private static void start(final Credentials credentials) {
    try {
      final NtlmPasswordAuthentication authentication =
          new NtlmPasswordAuthentication(null, credentials.getUser(), credentials.getPswd());
      final SmbFile dir = new SmbFile(credentials.getUrl(), authentication);
      System.out.println("Showing content of " + credentials.getUrl());
      for (final SmbFile file : dir.listFiles()) {
        System.out.println(file.getName());
      }
    } catch (final Exception ex) {
      ex.printStackTrace();
    }
  }

  public static void main(final String[] args) {
    try {
      start(Credentials.fromArgs(args));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private static class Credentials {
    private final String url;
    private final String user;
    private final String pswd;

    private Credentials(String url, String user, String pswd) {
      super();
      if (!url.startsWith("smb://")) {
        throw new IllegalArgumentException("Invalid url: not a samba url");
      }
      this.url = url;
      this.user = user;
      this.pswd = pswd;
    }

    public String getUrl() {
      return url;
    }

    public String getUser() {
      return user;
    }

    public String getPswd() {
      return pswd;
    }

    public static Credentials fromArgs(final String... args) {
      if (args == null || args.length != 3) {
        throw new IllegalArgumentException(
            "Please specify the samba url, username and password from command line");

      }
      return new Credentials(args[0], args[1], args[2]);
    }
  }
}
