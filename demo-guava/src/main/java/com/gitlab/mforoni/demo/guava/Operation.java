package com.gitlab.mforoni.demo.guava;

import com.google.common.base.Stopwatch;

abstract class Operation {

	public void test() {
		final Stopwatch stopwatch = Stopwatch.createStarted();
		execute();
		System.out.println("Took: " + stopwatch.stop());
	}

	abstract void execute();
}
