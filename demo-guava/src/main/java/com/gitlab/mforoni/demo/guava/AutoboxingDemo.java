package com.gitlab.mforoni.demo.guava;

final class AutoboxingDemo {

	// private static final int N = Integer.MAX_VALUE;
	private static final int N = 300000000;

	private AutoboxingDemo() {}

	public static void main(final String[] args) {
		new Operation() {

			@Override
			void execute() {
				Long sum = 0L;
				for (long i = 0; i < N; i++) {
					sum += i;
				}
				System.out.println(sum);
			}
		}.test();
		new Operation() {

			@Override
			void execute() {
				long sum = 0L;
				for (long i = 0; i < N; i++) {
					sum += i;
				}
				System.out.println(sum);
			}
		}.test();
	}
}