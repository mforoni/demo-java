package com.gitlab.mforoni.demo.guava;

import static com.google.common.io.Files.createParentDirs;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import com.google.common.io.Files;

final class FilesDemo {

	private static void start() throws IOException {
		final String pathname = "output/test/empty.txt";
		final File file = new File(pathname);
		createParentDirs(file);
		Files.touch(file);
		System.out.println("Create file " + file);
		final String extension = Files.getFileExtension(pathname);
		System.out.println("Extension = " + extension);
		Files.write("Remember the name", file, Charset.forName("utf-8"));
		final List<String> lines = Files.readLines(file, Charset.forName("utf-8"));
		System.out.println(lines);
	}

	public static void main(final String[] args) {
		try {
			start();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}
}
