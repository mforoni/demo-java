package com.gitlab.mforoni.demo.guava;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Testing searching performance of small set of 1-10 objects versus small list
 * 
 * @author Foroni
 */
final class SearchingInSmallSetDemo {

	private final Set<String> set = new HashSet<String>();
	private final List<String> list = new ArrayList<String>();

	private SearchingInSmallSetDemo() {}

	private void start(final int size, final int length) {
		final String[] keys = new String[size];
		for (int i = 0; i < size; i++) {
			keys[i] = getRandomString(length);
		}
		for (int i = 0; i < size; i++) {
			set.add(keys[i]);
		}
		for (int i = 0; i < size; i++) {
			list.add(keys[i]);
		}
		new Operation() {

			@Override
			void execute() {
				for (int i = 0; i < size; i++) {
					set.contains(keys[i]);
				}
			}
		}.test();
		new Operation() {

			@Override
			void execute() {
				for (int i = 0; i < size; i++) {
					list.contains(keys[i]);
				}
			}
		}.test();;
	}

	private String getRandomString(final int length) {
		final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		final StringBuilder sb = new StringBuilder();
		final Random rnd = new Random();
		for (int i = 0; i < length; i++) { // length of the random string.
			final int index = (int) (rnd.nextFloat() * alphabet.length());
			sb.append(alphabet.charAt(index));
		}
		return sb.toString();
	}

	public static void main(final String[] args) {
		new SearchingInSmallSetDemo().start(12, 9);
	}
}
