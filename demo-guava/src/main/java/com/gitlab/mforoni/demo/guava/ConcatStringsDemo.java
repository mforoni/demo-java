package com.gitlab.mforoni.demo.guava;

/**
 * Testing the fastest way to concatenate n strings. See:
 * http://stackoverflow.com/questions/5076740/whats-the-fastest-way-to-concatenate-two-strings-in-java
 * 
 * @author Foroni
 */
final class ConcatStringsDemo {

	private ConcatStringsDemo() {}

	private static void start(final String... strings) {
		new Operation() {

			@Override
			void execute() {
				String out = "";
				for (int i = 0; i < strings.length; i++) {
					out = out + strings[i];
				}
				System.out.println(out);
			}
		}.test();
		new Operation() {

			@Override
			void execute() {
				final StringBuilder sb = new StringBuilder();
				for (int i = 0; i < strings.length; i++) {
					sb.append(strings[i]);
				}
				System.out.println(sb.toString());
			}
		}.test();
		new Operation() {

			@Override
			void execute() {
				String output = "";
				for (int i = 0; i < strings.length; i++) {
					output = output.concat(strings[i]);
				}
				System.out.println(output);
			}
		}.test();;
	}

	public static void main(final String[] args) {
		start("casa", "caio", "string molto lunga, veramente lunga", "sole", "mare");
	}
}
