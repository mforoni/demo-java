package com.gitlab.mforoni.demo.guava;

import java.util.Arrays;
import com.google.common.base.Joiner;

final class JoinerDemo {

	private JoinerDemo() {}

	private static void run() {
		final String joinedNames = Joiner.on(", ").join(Arrays.asList("Mario", "Marco", "Francesco"));
		System.out.println(joinedNames);
		final String[] empty = new String[] {};
		final String joinedEmpty = Joiner.on(", ").join(empty);
		System.out.println(joinedEmpty);
		final String joinedNulls = Joiner.on(", ").skipNulls().join(Arrays.asList(null, "Mario", null, null, //
				"Marco", null, "Francesco"));
		System.out.println(joinedNulls);
		final String join = Joiner.on(' ').join("This", "text", "is obtained with guava joiner", ',', " contains char and numbers like", 1, 2, 3);
		System.out.println(join);;
	}

	public static void main(final String[] args) {
		run();
	}
}
