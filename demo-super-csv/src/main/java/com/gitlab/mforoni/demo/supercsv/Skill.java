package com.gitlab.mforoni.demo.supercsv;

public class Skill {

  private String name;
  private int spCost;
  private Integer damage; // (mt)
  private Integer rangeRng; // (rng)
  private String effect;
  private Boolean exclusive;
  private String type;
  private String weaponType;
  private Integer range;
  private String inheritRestriction;
  private Integer cooldown;

  public Skill() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getSpCost() {
    return spCost;
  }

  public void setSpCost(int spCost) {
    this.spCost = spCost;
  }

  public Integer getDamage() {
    return damage;
  }

  public void setDamage(Integer damage) {
    this.damage = damage;
  }

  public Integer getRangeRng() {
    return rangeRng;
  }

  public void setRangeRng(Integer rangeRng) {
    this.rangeRng = rangeRng;
  }

  public String getEffect() {
    return effect;
  }

  public void setEffect(String effect) {
    this.effect = effect;
  }

  public Boolean getExclusive() {
    return exclusive;
  }

  public void setExclusive(Boolean exclusive) {
    this.exclusive = exclusive;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getWeaponType() {
    return weaponType;
  }

  public void setWeaponType(String weaponType) {
    this.weaponType = weaponType;
  }

  public Integer getRange() {
    return range;
  }

  public void setRange(Integer range) {
    this.range = range;
  }

  public String getInheritRestriction() {
    return inheritRestriction;
  }

  public void setInheritRestriction(String inheritRestriction) {
    this.inheritRestriction = inheritRestriction;
  }

  public Integer getCooldown() {
    return cooldown;
  }

  public void setCooldown(Integer cooldown) {
    this.cooldown = cooldown;
  }

  @Override
  public String toString() {
    return "Skill [name=" + name + ", spCost=" + spCost + ", damage=" + damage + ", rangeRng="
        + rangeRng + ", effect=" + effect + ", exclusive=" + exclusive + ", type=" + type
        + ", weaponType=" + weaponType + ", range=" + range + ", inheritRestriction="
        + inheritRestriction + ", cooldown=" + cooldown + "]";
  }
}
