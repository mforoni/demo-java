package com.gitlab.mforoni.demo.supercsv;

import java.io.FileReader;
import java.io.IOException;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseBool;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

final class DemoSuperCSV {

  /**
   * An example of reading using CsvBeanReader.
   * 
   * @throws IOException
   */
  private static void readWithCsvBeanReader(final String csvPath) throws IOException {
    ICsvBeanReader beanReader = null;
    try {
      beanReader = new CsvBeanReader(new FileReader(csvPath), CsvPreference.STANDARD_PREFERENCE);
      // the header elements are used to map the values to the bean (names must match)
      final String[] header = beanReader.getHeader(true);
      final CellProcessor[] processors = getProcessors();
      Skill skill;
      while ((skill = beanReader.read(Skill.class, header, processors)) != null) {
        System.out.println(String.format("line=%s, row=%s, skill=%s", beanReader.getLineNumber(),
            beanReader.getRowNumber(), skill));
      }
    } finally {
      if (beanReader != null) {
        beanReader.close();
      }
    }
  }

  /**
   * Sets up the processors used for the examples. There are 11 CSV columns, so 11 processors are
   * defined. Empty columns are read as null (hence the NotNull() for mandatory columns).
   * 
   * @return the cell processors
   */
  private static CellProcessor[] getProcessors() {
    final CellProcessor[] processors = new CellProcessor[] { //
        new NotNull(), // name
        new Optional(new ParseInt()), // spCost
        new Optional(new ParseInt()), // damage
        new Optional(new ParseInt()), // rangeRng
        new NotNull(), // effect
        new Optional(new ParseBool("Yes", "No")), // exclusive
        new NotNull(), // type
        new Optional(), // weaponType
        new Optional(new ParseInt()), // range
        new Optional(), // inheritRestriction
        new Optional(new ParseInt()), // cooldown
    };
    return processors;
  }

  public static void main(String[] args) {
    try {
      readWithCsvBeanReader("resources/Skills.csv");
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }
}
