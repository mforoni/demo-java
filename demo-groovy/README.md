# demo-grovy

Demo project to test the integration of the Groovy programming language inside Java applications.

## About Groovy

Apache [Groovy](http://groovy-lang.org/index.html) is a powerful, optionally typed and dynamic language, with static-typing and static compilation capabilities, for the Java platform aimed at improving developer productivity thanks to a concise, familiar and easy to learn syntax. It offers scripting capabilities, Domain-Specific Language authoring, runtime and compile-time meta-programming and functional programming.