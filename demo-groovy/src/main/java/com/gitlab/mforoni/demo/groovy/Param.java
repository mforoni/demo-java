package com.gitlab.mforoni.demo.groovy;

final class Param<T> {

  private final String name;
  private final T value;

  Param(final String name, final T value) {
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public T getValue() {
    return value;
  }
}
