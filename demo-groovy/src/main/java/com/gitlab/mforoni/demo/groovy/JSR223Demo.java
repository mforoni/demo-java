package com.gitlab.mforoni.demo.groovy;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

final class JSR223Demo {

  private JSR223Demo() {}

  public void start() {
    try {
      final ScriptEngineManager manager = new ScriptEngineManager();
      // 1
      executeScript(manager, "(1..10).sum()");
      // 2
      final Param<String> first = new Param<>("first", "HELLO");
      final Param<String> second = new Param<>("second", "world");
      executeScript(manager, "first.toLowerCase() + ' ' + second.toUpperCase()", first, second);
      // 3
      executeScript(manager, "2*8");
      // 4
      final Param<Integer> foo = new Param<>("foo", 2);
      executeScript(manager, "foo*8", foo);
      // 5
      final Calendar cal = Calendar.getInstance();
      cal.set(1978, 0, 11);
      final Person persona = new Person("luigi", cal.getTime(), true, 22, 405.23);
      System.out.println(persona.toString());
      final Param<Person> p = new Param<>("p", persona);
      try {
        executeScript(manager, "p.name = \"mario\"\np.years=33", p);
      } catch (final ScriptException ex) {
        System.out.println(ex.getMessage() + "\n");
      }
      // 6
      final MutablePerson mutablePerson =
          new MutablePerson("luigi", cal.getTime(), true, 22, 405.23, new ArrayList<Car>());
      System.out.println(mutablePerson.toString());
      final Param<MutablePerson> mp = new Param<>("mp", mutablePerson);
      executeScript(manager, "mp.name = \"mario\"\nmp.years=33\nreturn mp", mp);
      // 7
      final List<Car> cars = new ArrayList<>();
      Collections.addAll(cars, new Car("Ford", "CD786GY", 75), new Car("Audi", "EW788BR", 79));
      final Param<MutablePerson> mutp = new Param<>("mutp",
          new MutablePerson("Francesco", cal.getTime(), false, 55, 234.4, cars));
      executeScript(manager, "return mutp.cars.find {c -> c.model == 'Ford'}", mutp);
    } catch (final Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Immutable (non modificabile anche in Groovy!)
   * 
   * @author Foroni
   */
  class Person {

    private final String name;
    private final Date birthDate;
    private final boolean visible;
    private final int years;
    private final Double money;

    Person(final String name, final Date birthDate, final boolean visible, final int years,
        final Double money) {
      this.name = name;
      this.birthDate = birthDate;
      this.visible = visible;
      this.years = years;
      this.money = money;
    }

    public String getName() {
      return name;
    }

    public Date getBirthDate() {
      return birthDate;
    }

    public boolean isVisible() {
      return visible;
    }

    public int getYears() {
      return years;
    }

    public Double getMoney() {
      return money;
    }

    @Override
    public String toString() {
      return "Persona: " + name + ", " + years + ", " + birthDate + ", " + money + ", " + visible;
    }
  }

  /**
   * Mutable class Persona
   * 
   * @author Foroni
   */
  class MutablePerson {

    private String name;
    private Date birthDate;
    private boolean visible;
    private int years;
    private Double money;
    private List<Car> cars;

    private MutablePerson(final String name, final Date birthDate, final boolean visible,
        final int years, final Double money, final List<Car> cars) {
      this.name = name;
      this.birthDate = birthDate;
      this.visible = visible;
      this.years = years;
      this.money = money;
      this.cars = cars;
    }

    public String getName() {
      return name;
    }

    public void setName(final String name) {
      this.name = name;
    }

    public Date getBirthDate() {
      return birthDate;
    }

    public void setBirthDate(final Date birthDate) {
      this.birthDate = birthDate;
    }

    public boolean isVisible() {
      return visible;
    }

    public void setVisible(final boolean visible) {
      this.visible = visible;
    }

    public int getYears() {
      return years;
    }

    public void setYears(final int years) {
      this.years = years;
    }

    public Double getMoney() {
      return money;
    }

    public void setMoney(final Double money) {
      this.money = money;
    }

    public List<Car> getCars() {
      return cars;
    }

    public void setCars(final List<Car> cars) {
      this.cars = cars;
    }

    @Override
    public String toString() {
      return "Persona: " + name + ", " + years + ", " + birthDate + ", " + money + ", " + visible;
    }
  }

  class Car {

    private String model;
    private String plate;
    private int kw;

    public Car(final String model, final String plate, final int kw) {
      super();
      this.model = model;
      this.plate = plate;
      this.kw = kw;
    }

    public String getModel() {
      return model;
    }

    public void setModel(final String model) {
      this.model = model;
    }

    public String getPlate() {
      return plate;
    }

    public void setPlate(final String plate) {
      this.plate = plate;
    }

    public int getKw() {
      return kw;
    }

    public void setKw(final int kw) {
      this.kw = kw;
    }

    @Override
    public String toString() {
      return "Car: " + model + ", " + plate + ", " + kw;
    }
  }

  private void executeScript(final ScriptEngineManager manager, final String script)
      throws ScriptException {
    executeScript(manager, script, new Param<?>[0]);
  }

  private void executeScript(final ScriptEngineManager manager, final String script,
      final Param<?>... params) throws ScriptException {
    final ScriptEngine engine = manager.getEngineByName("groovy");
    if (params != null && params.length > 0) {
      for (final Param<?> param : params) {
        engine.put(param.getName(), param.getValue());
      }
    }
    System.out.println(String.format("Script: %s", script));
    final Object value = engine.eval(script);
    GroovyDemo.printReturnedValue(value);
  }

  public static void main(final String[] args) throws ScriptException {
    new JSR223Demo().start();
  }
}
