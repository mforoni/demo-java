package com.gitlab.mforoni.demo.groovy;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.MissingPropertyException;

final class GroovyDemo {

  private GroovyDemo() {}

  public static void printReturnedValue(final Object value) {
    if (value == null) {
      System.out.println("Returned value is null\n");
    } else {
      System.out.println(String.format("Returned value type = %s, value = %s\n",
          value.getClass().getSimpleName(), value.toString()));
    }
  }

  public void start() {
    executeGroovyShell("return true");
    executeGroovyShell("return 2*4");
    final Param<Integer> p = new Param<>("foo", new Integer(2));
    try {
      executeGroovyShell("println $foo*4", p);
    } catch (final MissingPropertyException ex) {
      System.out.println(ex.getMessage() + "\n");
    }
    executeGroovyShell("return \"$foo\"", p);
    executeGroovyShell("println '$foo*4'", p);
    executeGroovyShell("println \"$foo*4\"", p);
    executeGroovyShell("return \"$foo*4\"", p);
    executeGroovyShell("return \"$foo\"*4", p);
  }

  private <T> void executeGroovyShell(final String script) {
    executeGroovyShell(script, null);
  }

  private <T> void executeGroovyShell(final String script, final Param<T> param) {
    // call groovy expressions from Java code
    GroovyShell shell = null;
    if (param != null) {
      final Binding binding = new Binding();
      binding.setVariable(param.getName(), param.getValue());
      shell = new GroovyShell(binding);
    } else {
      shell = new GroovyShell();
    }
    System.out.println(String.format("Script: %s", script));
    final Object value = shell.evaluate(script);
    printReturnedValue(value);
  }

  public static void main(final String[] args) {
    new GroovyDemo().start();
  }
}
