# Java APIs Demos

Contains demos & snippets of Java SDK and various third-party APIs:

* demo-common-csv
* demo-groovy
* demo-guava
* demo-javapoet
* demo-jcifs
* demo-jdk5
* demo-jdk7
* demo-jeval
* demo-jgrapht
* demo-joda-time
* demo-jsoup
* demo-log4j2
* demo-logback
* demo-maven
* demo-mockito
* demo-owner
* demo-string-similarity
* demo-super-csv
* effective-java

## About [Commons CSV](http://commons.apache.org/proper/commons-csv/)

Commons CSV reads and writes files in variations of the Comma Separated Value (CSV) format.

## About [Groovy](http://groovy-lang.org/)

Apache Groovy is a powerful, optionally typed and dynamic language, with static-typing and static compilation capabilities, for the Java platform aimed at improving developer productivity thanks to a concise, familiar and easy to learn syntax. It offers scripting capabilities, Domain-Specific Language authoring, runtime and compile-time meta-programming and functional programming.

## About [Guava](https://github.com/google/guava)

Guava is a set of core libraries that includes new collection types (such as multimap and multiset), immutable collections, a graph library, functional types, an in-memory cache, and APIs/utilities for concurrency, I/O, hashing, primitives, reflection, string processing, and much more!

## About [JCIFS](https://jcifs.samba.org/)

JCIFS is an Open Source client library that implements the CIFS/SMB networking protocol in 100% Java. CIFS is the standard file sharing protocol on the Microsoft Windows platform (e.g. Map Network Drive ...).

## About [JEval](http://jeval.sourceforge.net/)

*JEval* is the advanced library for adding high-performance, mathematical, Boolean and functional expression parsing and evaluation to your Java applications. 

## About [Joda-Time](http://www.joda.org/joda-time/)

Joda-Time provides a quality replacement for the Java date and time classes.
Joda-Time is the de facto standard date and time library for Java prior to Java SE 8. Users are now asked to migrate to java.time (JSR-310).

## About [Log4j2](https://logging.apache.org/log4j/2.x/)

Apache Log4j 2 is an upgrade to Log4j that provides significant improvements over its predecessor, Log4j 1.x, and provides many of the improvements available in Logback while fixing some inherent problems in Logback's architecture.

## About [Logback](https://logback.qos.ch/)

Logback is intended as a successor to the popular log4j project, picking up where log4j leaves off.

## About [Mockito](https://site.mockito.org/)

Mockito is a mocking framework, JAVA-based library that is used for effective unit testing of JAVA applications. Mockito is used to mock interfaces so that a dummy functionality can be added to a mock interface that can be used in unit testing.

## About [Super CSV](http://super-csv.github.io/super-csv/)

Super CSV is a fast, programmer-friendly, open-source library for reading and writing CSV files with Java.


## TODO

* test [guice](https://github.com/google/guice)