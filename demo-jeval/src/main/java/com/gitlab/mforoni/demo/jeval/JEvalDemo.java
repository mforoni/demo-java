package com.gitlab.mforoni.demo.jeval;

import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;

final class JEvalDemo {

  private JEvalDemo() {}

  private void start() {
    try {
      final Evaluator evaluator = new Evaluator();
      evaluate(evaluator, "1 || 0");
      evaluate(evaluator, "1 && (0 || 1)");
      // evaluate(evaluator, "T && (F || T)"); errore
      evaluate(evaluator, "'ALTA' != 'ALTA'");
      // evaluate(evaluator, "null == null"); errore
      evaluate(evaluator, "'null' == 'null'");
    } catch (final EvaluationException ex) {
      ex.printStackTrace();
    }
  }

  private void evaluate(final Evaluator evaluator, final String expr) throws EvaluationException {
    final String result = evaluator.evaluate(expr);
    System.out.println(expr + " returns " + result);
  }

  public static void main(final String[] args) {
    new JEvalDemo().start();
  }
}
