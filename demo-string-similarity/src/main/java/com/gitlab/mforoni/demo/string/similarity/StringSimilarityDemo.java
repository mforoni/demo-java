package com.gitlab.mforoni.demo.string.similarity;

import info.debatty.java.stringsimilarity.Damerau;
import info.debatty.java.stringsimilarity.Levenshtein;
import info.debatty.java.stringsimilarity.NormalizedLevenshtein;
import info.debatty.java.stringsimilarity.interfaces.StringDistance;

public class StringSimilarityDemo {

  private static void testMetric(final StringDistance metric, final String s1, final String s2) {
    final double distance = metric.distance(s1, s2);
    System.out.println(String.format("Distance between %s, %s with %s is %s", s1, s2,
        metric.getClass().getSimpleName(), distance));
  }

  private static void test() {
    final Levenshtein levenshtein = new Levenshtein();
    final NormalizedLevenshtein normalizedLevenshtein = new NormalizedLevenshtein();
    final Damerau damerau = new Damerau();
    testMetric(levenshtein, "My string", "My $tring");
    testMetric(damerau, "My string", "My $tring");
    testMetric(normalizedLevenshtein, "My string", "My $tring");
    testMetric(damerau, "ABCDEF", "ABDCEF"); // 1 substitution
    testMetric(damerau, "ABCDEF", "BACDFE"); // 2 substitutions
    testMetric(damerau, "ABCDEF", "ABCDE"); // 1 deletion
    testMetric(damerau, "ABCDEF", "BCDEF"); // 1 deletion
    testMetric(damerau, "ABCDEF", "ABCGDEF"); // 1 deletion
    testMetric(damerau, "ABCDEF", "POIU"); // All different
  }

  public static void main(final String[] args) {
    test();
  }
}
