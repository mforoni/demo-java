package com.gitlab.mforoni.demo.owner;

import org.aeonbits.owner.ConfigFactory;


final class OwnerDemo {

  private OwnerDemo() {}

  public static void main(String[] args) {
    final ServerConfig serverConfig = ConfigFactory.create(ServerConfig.class);
    System.out.println(serverConfig);
  }
}
