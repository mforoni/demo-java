package com.gitlab.mforoni.demo.owner;

import org.aeonbits.owner.Config;

public interface ServerConfig extends Config {
  int port();

  String hostname();

  int maxThreads();

}