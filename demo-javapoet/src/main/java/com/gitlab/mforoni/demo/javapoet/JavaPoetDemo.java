package com.gitlab.mforoni.demo.javapoet;

import java.io.File;
import java.io.IOException;
import javax.lang.model.element.Modifier;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

final class JavaPoetDemo {

  private static final String LAST_NAME = "lastName";
  private static final String FIRST_NAME = "firstName";
  private static final String PACKAGE_NAME = "com.gitlab.mforoni.demo.javapoet";
  private static final String PATHNAME = "src/main/java/";

  private JavaPoetDemo() {}

  private static void writeToFile(final JavaFile javaFile, final String className)
      throws IOException {
    javaFile.writeTo(new File(PATHNAME));
    System.out.println("File " + className + ".java successfully written at " + PATHNAME);
  }

  private static void testBean() throws IOException {
    final MethodSpec constructor = MethodSpec.constructorBuilder().addModifiers(Modifier.PUBLIC). //
        addParameter(String.class, FIRST_NAME). //
        addParameter(String.class, LAST_NAME). //
        addStatement("this.$N = $N", FIRST_NAME, FIRST_NAME). //
        addStatement("this.$N = $N", LAST_NAME, LAST_NAME).build();
    final String className = "Person";
    final TypeSpec person = TypeSpec.classBuilder(className).addModifiers(Modifier.PUBLIC). //
        addField(String.class, FIRST_NAME, Modifier.PRIVATE, Modifier.FINAL). //
        addField(String.class, LAST_NAME, Modifier.PRIVATE, Modifier.FINAL). //
        addMethod(constructor).build();
    final JavaFile javaFile = JavaFile.builder(PACKAGE_NAME, person).build();
    writeToFile(javaFile, className);
  }

  private static void testHelloWorld() throws IOException {
    final MethodSpec main =
        MethodSpec.methodBuilder("main").addModifiers(Modifier.PUBLIC, Modifier.STATIC)
            .returns(void.class).addParameter(String[].class, "args")
            .addStatement("$T.out.println($S)", System.class, "Hello, JavaPoet!")
            .addStatement("System.out.println($S + $N)", "The sum of the first 10 number is ",
                "sumOf(10)")
            .build();
    final MethodSpec sumOf = MethodSpec.methodBuilder("sumOf").returns(int.class)
        .addModifiers(Modifier.STATIC).addParameter(int.class, "n", Modifier.FINAL). //
        addStatement("int sum = 0"). //
        beginControlFlow("for (int i = 1; i <= n; i++)"). //
        addStatement("sum += i"). //
        endControlFlow(). //
        addStatement("return sum").build();
    final String className = "HelloWorld";
    final TypeSpec helloWorld = TypeSpec.classBuilder(className)
        .addModifiers(Modifier.PUBLIC, Modifier.FINAL).addMethod(sumOf).addMethod(main).build();
    final JavaFile javaFile = JavaFile.builder(PACKAGE_NAME, helloWorld).indent("\t").build();
    writeToFile(javaFile, className);
  }

  public static void main(final String[] args) {
    try {
      testHelloWorld();
      testBean();
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }
}
