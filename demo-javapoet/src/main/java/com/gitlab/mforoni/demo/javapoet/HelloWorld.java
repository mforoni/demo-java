package com.gitlab.mforoni.demo.javapoet;

public final class HelloWorld {
  static int sumOf(final int n) {
    int sum = 0;
    for (int i = 1; i <= n; i++) {
      sum += i;
    }
    return sum;
  }

  public static void main(String[] args) {
    System.out.println("Hello, JavaPoet!");
    System.out.println("The sum of the first 10 number is " + sumOf(10));
  }
}
