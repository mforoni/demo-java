package com.gitlab.mforoni.demo.jgrapht;

import org.jgrapht.Graph;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

final class JGraphTDemo {

  static final String PCPOLIZZA = "PCPOLIZZA";
  static final String PCPOLIZZABENE = "PCPOLIZZABENE";
  static final String PCBENEASS = "PCBENEASS";

  static void buildDemoGraph(final Graph<String, DefaultEdge> g) {
    g.addVertex(PCPOLIZZA);
    g.addVertex(PCBENEASS);
    g.addVertex(PCPOLIZZABENE);
    g.addEdge(PCPOLIZZA, PCPOLIZZABENE);
    g.addEdge(PCPOLIZZABENE, PCBENEASS);
  }

  public static void main(final String[] args) {
    new JGraphTDemo();
  }

  JGraphTDemo() {
    final UndirectedGraph<String, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
    buildDemoGraph(g);
    System.out.println(g.toString());
  }
}
