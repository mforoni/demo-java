/*
 * ========================================== JGraphT : a free Java graph-theory library
 * ==========================================
 *
 * Project Info: http://jgrapht.sourceforge.net/ Project Creator: Barak Naveh
 * (http://sourceforge.net/users/barak_naveh)
 *
 * (C) Copyright 2003-2008, by Barak Naveh and Contributors.
 *
 * This program and the accompanying materials are dual-licensed under either
 *
 * (a) the terms of the GNU Lesser General Public License version 2.1 as published by the Free
 * Software Foundation, or (at your option) any later version.
 *
 * or (per the licensee's choosing)
 *
 * (b) the terms of the Eclipse Public License v1.0 as published by the Eclipse Foundation.
 */
/*
 * ---------------------- JGraphAdapterDemo.java ---------------------- (C) Copyright 2003-2008, by
 * Barak Naveh and Contributors.
 *
 * Original Author: Barak Naveh Contributor(s): -
 *
 * $Id$
 *
 * Changes ------- 03-Aug-2003 : Initial revision (BN); 07-Nov-2003 : Adaptation to JGraph 3.0 (BN);
 *
 */
package com.gitlab.mforoni.demo.jgrapht;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Rectangle2D;
import javax.swing.JApplet;
import javax.swing.JFrame;
import org.jgraph.JGraph;
import org.jgraph.graph.AttributeMap;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;
import org.jgrapht.DirectedGraph;
import org.jgrapht.ListenableGraph;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultListenableGraph;
import org.jgrapht.graph.DirectedMultigraph;

/**
 * A demo applet that shows how to use JGraph to visualize JGraphT graphs.
 *
 * @author Barak Naveh
 * @since Aug 3, 2003
 */
public class JGraphAdapterDemo extends JApplet {

  private static final long serialVersionUID = 3256444702936019250L;
  private static final Color DEFAULT_BG_COLOR = Color.decode("#FAFBFF");
  private static final Dimension DEFAULT_SIZE = new Dimension(530, 320);
  static final String PCPOLIZZA = "PCPOLIZZA";
  static final String PCPOLIZZABENE = "PCPOLIZZABENE";
  static final String PCBENEASS = "PCBENEASS";

  /**
   * a listenable directed multigraph that allows loops and parallel edges.
   */
  private static class ListenableDirectedMultigraph<V, E> extends DefaultListenableGraph<V, E>
      implements DirectedGraph<V, E> {

    private static final long serialVersionUID = 1L;

    ListenableDirectedMultigraph(final Class<E> edgeClass) {
      super(new DirectedMultigraph<V, E>(edgeClass));
    }
  }

  /**
   * An alternative starting point for this demo, to also allow running this applet as an
   * application.
   *
   * @param args ignored.
   */
  public static void main(final String[] args) {
    final JGraphAdapterDemo applet = new JGraphAdapterDemo();
    applet.init();
    final JFrame frame = new JFrame();
    frame.getContentPane().add(applet);
    frame.setTitle("JGraphT Adapter to JGraph Demo");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }

  private JGraphModelAdapter<String, DefaultEdge> jgAdapter;

  private void adjustDisplaySettings(final JGraph jg) {
    jg.setPreferredSize(DEFAULT_SIZE);
    Color c = DEFAULT_BG_COLOR;
    final String colorStr = getParameter("bgcolor");
    if (colorStr != null) {
      c = Color.decode(colorStr);
    }
    jg.setBackground(c);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void init() {
    // create a JGraphT graph
    final ListenableGraph<String, DefaultEdge> g =
        new ListenableDirectedMultigraph<>(DefaultEdge.class);
    // create a visualization using JGraph, via an adapter
    jgAdapter = new JGraphModelAdapter<>(g);
    final JGraph jgraph = new JGraph(jgAdapter);
    adjustDisplaySettings(jgraph);
    getContentPane().add(jgraph);
    resize(DEFAULT_SIZE);
    g.addVertex(PCPOLIZZA);
    g.addVertex(PCBENEASS);
    g.addVertex(PCPOLIZZABENE);
    g.addEdge(PCPOLIZZA, PCPOLIZZABENE);
    g.addEdge(PCPOLIZZABENE, PCBENEASS);
    // position vertices nicely within JGraph component
    positionVertexAt(PCPOLIZZA, 130, 40);
    positionVertexAt(PCBENEASS, 60, 200);
    positionVertexAt(PCPOLIZZABENE, 310, 230);
    // positionVertexAt(v4, 380, 70);
    // that's all there is to it!...
  }

  @SuppressWarnings("unchecked") // FIXME hb 28-nov-05: See FIXME below
  private void positionVertexAt(final Object vertex, final int x, final int y) {
    final DefaultGraphCell cell = jgAdapter.getVertexCell(vertex);
    final AttributeMap attr = cell.getAttributes();
    final Rectangle2D bounds = GraphConstants.getBounds(attr);
    final Rectangle2D newBounds =
        new Rectangle2D.Double(x, y, bounds.getWidth(), bounds.getHeight());
    GraphConstants.setBounds(attr, newBounds);
    // TODO: Clean up generics once JGraph goes generic
    final AttributeMap cellAttr = new AttributeMap();
    cellAttr.put(cell, attr);
    jgAdapter.edit(cellAttr, null, null, null);
  }
}
